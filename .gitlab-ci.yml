image: python:3.11-slim

workflow:
  rules:
    - when: always

stages:
  - pure sql
  - python

services:
  - name: postgres:15-alpine
    alias: postgres

variables:
  POSTGRES_DB: postgres
  POSTGRES_USER: postgres
  POSTGRES_PASSWORD: password
  POSTGRES_HOST_AUTH_METHOD: trust

  DBNAME: postgres
  DBHOST: postgres
  DBPORT: 5432
  DBUSER: postgres
  DBPASSWORD: password

  PROJECT_DIR: ${CI_PROJECT_DIR}
  SCRIPTS_DIR: ${PROJECT_DIR}/scripts
  UTILS_DIR: ${PROJECT_DIR}/utils
  PYPSQL: ${UTILS_DIR}/pypsql.py

  TEST_DIR: tests/

  NO_COLOUR: '\033[0m'
  LIGHT_RED: '\033[1;31m'

.setup:
  before_script:
    - pip install -r ${PROJECT_DIR}/requirements.txt > /dev/null 2> /dev/null


.print-data:
  script:
    - echo "Table 'project.petrol_stations' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.petrol_stations LIMIT ${LIMIT}"
      --verbose
    - echo "Table 'project.positions' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.positions LIMIT
      ${LIMIT}" --verbose
    - echo "Table 'project.employees' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.employees LIMIT
      ${LIMIT}" --verbose
    - echo "Table 'project.petrol_pumps' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM
      project.petrol_pumps LIMIT ${LIMIT}" --verbose
    - echo "Table 'project.fuel' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.fuel
      LIMIT ${LIMIT}" --verbose
    - echo "Table 'project.fuel_in_pump' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.fuel_in_pump LIMIT
      ${LIMIT}" --verbose
    - echo "Table 'project.fuel_presence' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.fuel_presence LIMIT
      ${LIMIT}" --verbose

.print-views:
  script:
    - echo "Table 'project.actual_fuel' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.actual_fuel LIMIT ${LIMIT}"
      --verbose
    - echo "Table 'project.high_level_employees' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.high_level_employees LIMIT
      ${LIMIT}" --verbose
    - echo "Table 'project.addresses' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.addresses LIMIT
      ${LIMIT}" --verbose
    - echo "Table 'project.presence_92' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM
      project.presence_92 LIMIT ${LIMIT}" --verbose
    - echo "Table 'project.unpersonalized_employees' sample:"
    - python3 ${PYPSQL} --sql="SELECT * FROM project.unpersonalized_employees
      LIMIT ${LIMIT}" --verbose


sql scripts:
  stage: pure sql
  extends:
    - .setup
  script:
    # Task 3
    - echo -e "${LIGHT_RED}>>> Task 3. DDL Queries <<<${NO_COLOUR}"
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/create_scripts.sql" --cat
    - echo -e "${LIGHT_RED}>>> Task 3. DML Queries <<<${NO_COLOUR}"
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/insert_scripts.sql" --cat
    - !reference [ .print-data, script ]
    # Task 4
    - echo -e "${LIGHT_RED}>>> Task 4. DML Queries <<<${NO_COLOUR}"
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/update_scripts.sql" --cat
    - !reference [ .print-data, script ]
    # Task 5
    - echo -e "${LIGHT_RED}>>> Task 5. Select Queries <<<${NO_COLOUR}"
    - |
      for i in ${SCRIPTS_DIR}/select_scripts/**/*.sql
      do
        python3 ${PYPSQL} --file="$i" --cat --verbose
      done
    # Task 6
    - echo -e "${LIGHT_RED}>>> Task 6.1 Create Indexes Queries <<<${NO_COLOUR}"
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/create_indexes.sql" --cat

    - echo -e "${LIGHT_RED}>>> Task 6.2 Create Views Queries <<<${NO_COLOUR}"
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/create_views.sql" --cat
    - !reference [ .print-views, script ]

    - echo -e "${LIGHT_RED}>>> Task 6.3 Create and Call Functions Queries <<<${NO_COLOUR}"
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/create_functions.sql" --cat
    - !reference [ .print-data, script ]

  variables:
    LIMIT: 5


orm-scripts:
  stage: python
  needs: []

  extends:
    - .setup

  script:
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/create_scripts.sql"
    - python3 ${PYPSQL} --file="${SCRIPTS_DIR}/create_functions.sql" --cat
    - python3 -m pytest ${TEST_DIR} -s -v