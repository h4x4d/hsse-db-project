set search_path to project;

-- Only high level employees
create view high_level_employees as
select *
from employees
where position_name like 'high level%';

-- Employees statistics without personal data
create view unpersonalized_employees as
select (rank() over w) as id, position_name, petrol_station_address
from employees
where position_name like 'high level%'
window w as (
    order by (name, surname)
    );

-- Only actual fuel info
create view actual_fuel as
  select id, name, fuel_cost from fuel where valid_to > now();

-- 92 in pump presence
create view presence_92 as
  select * from fuel_presence where fuel_name = '92';

-- list of addresses of stations
create view addresses as
  select address from petrol_stations;