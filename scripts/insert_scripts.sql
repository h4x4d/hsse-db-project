-- data generation tool - https://generatedata.com/generator

set search_path to project;

-- fuel

insert into fuel(name, fuel_cost, valid_from, valid_to)
values ('92', 40.0, 'epoch', '2020-01-01 00:00:00'),
       ('92', 43.5, '2020-01-01 00:00:00', '2022-03-01 05:00:00'),
       ('92', 47.6, '2022-03-01 05:00:00', '2023-05-11 11:11:11'),
       ('92', 51.0, '2023-05-11 11:11:11', '2999-01-01 00:00:00');

insert into fuel(name, fuel_cost, valid_from, valid_to)
values ('95', 43.0, 'epoch', '2020-01-01 00:00:00'),
       ('95', 44.6, '2020-01-01 00:00:00', '2022-03-01 05:00:00'),
       ('95', 50.6, '2022-04-01 05:00:00', '2023-06-11 11:11:11'),
       ('95', 55.1, '2023-06-11 11:11:11', '2999-01-01 00:00:00');

insert into fuel(name, fuel_cost, valid_from, valid_to)
values ('diesel', 41.0, 'epoch', '2020-01-01 00:00:00'),
       ('diesel', 40.6, '2020-01-01 00:00:00', '2022-03-01 05:00:00'),
       ('diesel', 45.6, '2022-04-01 05:00:00', '2023-06-11 11:11:11'),
       ('diesel', 50.1, '2023-06-11 11:11:11', '2999-01-01 00:00:00');

-- positions

insert into positions
values ('refueller', 15000),
       ('seller', 25000),
       ('cleaner', 20000),
       ('driver', 30000),
       ('waiter', 23000);

insert into positions
    (select concat('high level ', name), salary * 2 from positions);

-- petrol stations

insert into petrol_stations
values ('Ap #728-7257 Tincidunt Avenue', '8:31', '20:27', '1'),
       ('783-197 Montes, St.', '6:36', '21:07', '1'),
       ('P.O. Box 621, 2189 Dictum Street', '6:09', '18:24', '0'),
       ('Ap #502-3499 A, St.', '8:59', '19:41', '1'),
       ('Ap #593-4380 Nunc Avenue', '9:59', '20:18', '0'),
       ('441-2655 Velit. St.', '8:41', '18:32', '0'),
       ('421-2741 Djambo. St.', '0:00', '0:00', '0'),
       ('8046 Metus Street', '7:33', '22:51', '0'),
       ('Ap #872-9720 Porttitor Road', '7:31', '23:51', '0'),
       ('Ap #616-6338 Nec Rd.', '6:11', '21:40', '1'),
       ('Ap #123-6051 Augue Rd.', '8:40', '21:53', '0');

-- employees

insert into employees
values ('Laura', 'Logan', 'high level waiter', 'P.O. Box 621, 2189 Dictum Street'),
       ('Hilel', 'Haney', 'refueller', '783-197 Montes, St.'),
       ('Shelly', 'Atkins', 'high level cleaner', 'Ap #728-7257 Tincidunt Avenue'),
       ('Gray', 'Swanson', 'cleaner', 'Ap #502-3499 A, St.'),
       ('Jared', 'Watson', 'waiter', 'Ap #872-9720 Porttitor Road'),
       ('Rana', 'Ball', 'high level seller', '8046 Metus Street'),
       ('Pearl', 'Zamora', 'high level driver', 'Ap #502-3499 A, St.'),
       ('Honorato', 'Bender', 'high level cleaner', '783-197 Montes, St.'),
       ('Nathaniel', 'Oneil', 'high level waiter', 'Ap #502-3499 A, St.'),
       ('Galena', 'Mcclain', 'high level refueller', '783-197 Montes, St.'),
       ('John', 'Richard', 'high level waiter', '783-197 Montes, St.'),
       ('Xenos', 'Robinson', 'cleaner', 'Ap #593-4380 Nunc Avenue'),
       ('Murphy', 'Howe', 'high level waiter', 'Ap #872-9720 Porttitor Road'),
       ('Rylee', 'Riggs', 'high level driver', 'Ap #502-3499 A, St.'),
       ('Lillian', 'Barrett', 'driver', 'Ap #123-6051 Augue Rd.'),
       ('Donovan', 'Hess', 'seller', '8046 Metus Street'),
       ('Breanna', 'Baldwin', 'refueller', '783-197 Montes, St.'),
       ('Jason', 'Burris', 'high level cleaner', 'Ap #728-7257 Tincidunt Avenue'),
       ('Kathleen', 'Peck', 'high level driver', '441-2655 Velit. St.'),
       ('Burke', 'Nichols', 'refueller', '8046 Metus Street'),
       ('Lydia', 'Perkins', 'high level seller', 'P.O. Box 621, 2189 Dictum Street'),
       ('Ashely', 'Snow', 'high level cleaner', '783-197 Montes, St.'),
       ('Mary', 'Malone', 'driver', 'Ap #616-6338 Nec Rd.'),
       ('Colleen', 'Schroeder', 'refueller', 'Ap #593-4380 Nunc Avenue'),
       ('Clio', 'Bender', 'high level waiter', '441-2655 Velit. St.'),
       ('Austin', 'Cannon', 'driver', 'Ap #123-6051 Augue Rd.'),
       ('Kenneth', 'Bridges', 'high level refueller', '8046 Metus Street'),
       ('Bianca', 'Moody', 'high level seller', 'Ap #123-6051 Augue Rd.'),
       ('Glenna', 'Boyle', 'refueller', 'Ap #872-9720 Porttitor Road'),
       ('Rudyard', 'Emerson', 'high level waiter', 'Ap #728-7257 Tincidunt Avenue'),
       ('Jaden', 'Wilcox', 'high level waiter', '441-2655 Velit. St.'),
       ('Francis', 'Alford', 'waiter', 'Ap #728-7257 Tincidunt Avenue'),
       ('Rana', 'Cameron', 'seller', 'Ap #728-7257 Tincidunt Avenue'),
       ('Chanda', 'Garza', 'high level seller', 'Ap #593-4380 Nunc Avenue'),
       ('Larissa', 'Moon', 'waiter', '783-197 Montes, St.'),
       ('Griffith', 'Snyder', 'high level refueller', 'Ap #616-6338 Nec Rd.'),
       ('Acton', 'Conner', 'high level cleaner', 'P.O. Box 621, 2189 Dictum Street'),
       ('Samuel', 'Knox', 'waiter', '441-2655 Velit. St.'),
       ('Alea', 'Oneal', 'refueller', 'Ap #616-6338 Nec Rd.'),
       ('Irene', 'Flynn', 'seller', '441-2655 Velit. St.'),
       ('Jerome', 'Alvarado', 'high level driver', 'Ap #728-7257 Tincidunt Avenue'),
       ('Noble', 'Fitzpatrick', 'waiter', 'Ap #593-4380 Nunc Avenue'),
       ('Madison', 'Peck', 'high level driver', '8046 Metus Street'),
       ('Lacy', 'Casey', 'driver', 'Ap #872-9720 Porttitor Road'),
       ('Freya', 'Bell', 'driver', 'Ap #872-9720 Porttitor Road'),
       ('Yoshio', 'Jimenez', 'high level waiter', 'Ap #728-7257 Tincidunt Avenue'),
       ('Urielle', 'Britt', 'refueller', 'Ap #593-4380 Nunc Avenue'),
       ('Cedric', 'Parsons', 'high level refueller', 'Ap #872-9720 Porttitor Road'),
       ('Ashton', 'Burt', 'waiter', 'Ap #502-3499 A, St.'),
       ('Quentin', 'Roman', 'high level waiter', '441-2655 Velit. St.');

-- fuel pumps

insert into petrol_pumps(side, in_air, petrol_station_address)
values ('right', '1', 'Ap #728-7257 Tincidunt Avenue'),
       ('right', '1', '783-197 Montes, St.'),
       ('left', '1', 'Ap #872-9720 Porttitor Road'),
       ('right', '1', 'P.O. Box 621, 2189 Dictum Street'),
       ('left', '0', 'Ap #502-3499 A, St.'),
       ('left', '0', '783-197 Montes, St.'),
       ('right', '1', 'Ap #616-6338 Nec Rd.'),
       ('left', '0', '783-197 Montes, St.'),
       ('left', '0', '441-2655 Velit. St.'),
       ('right', '0', '441-2655 Velit. St.'),
       ('left', '0', '441-2655 Velit. St.'),
       ('right', '1', '8046 Metus Street'),
       ('left', '0', '783-197 Montes, St.'),
       ('right', '1', 'Ap #502-3499 A, St.'),
       ('left', '0', '441-2655 Velit. St.'),
       ('left', '1', 'P.O. Box 621, 2189 Dictum Street'),
       ('left', '1', 'Ap #123-6051 Augue Rd.'),
       ('right', '1', '8046 Metus Street'),
       ('right', '1', 'Ap #593-4380 Nunc Avenue'),
       ('right', '0', 'Ap #502-3499 A, St.'),
       ('right', '1', '783-197 Montes, St.'),
       ('left', '0', 'Ap #123-6051 Augue Rd.'),
       ('left', '0', '8046 Metus Street'),
       ('right', '0', '441-2655 Velit. St.'),
       ('right', '1', 'Ap #123-6051 Augue Rd.'),
       ('right', '0', '441-2655 Velit. St.'),
       ('left', '1', 'Ap #616-6338 Nec Rd.'),
       ('right', '0', 'Ap #616-6338 Nec Rd.'),
       ('right', '1', 'Ap #123-6051 Augue Rd.'),
       ('right', '1', 'Ap #872-9720 Porttitor Road'),
       ('right', '1', 'Ap #728-7257 Tincidunt Avenue'),
       ('left', '1', 'Ap #593-4380 Nunc Avenue'),
       ('right', '0', 'Ap #123-6051 Augue Rd.'),
       ('left', '1', 'Ap #728-7257 Tincidunt Avenue'),
       ('left', '0', '8046 Metus Street'),
       ('right', '0', '8046 Metus Street'),
       ('right', '1', '8046 Metus Street'),
       ('left', '0', '8046 Metus Street'),
       ('left', '1', '783-197 Montes, St.'),
       ('left', '0', 'Ap #616-6338 Nec Rd.'),
       ('right', '1', 'Ap #616-6338 Nec Rd.'),
       ('left', '0', 'Ap #616-6338 Nec Rd.'),
       ('right', '0', 'P.O. Box 621, 2189 Dictum Street'),
       ('right', '0', 'Ap #872-9720 Porttitor Road'),
       ('left', '0', '783-197 Montes, St.'),
       ('right', '0', 'Ap #593-4380 Nunc Avenue'),
       ('left', '1', 'P.O. Box 621, 2189 Dictum Street'),
       ('right', '1', 'Ap #728-7257 Tincidunt Avenue'),
       ('left', '0', '8046 Metus Street'),
       ('right', '1', 'Ap #728-7257 Tincidunt Avenue');


-- fuel_in_pump

insert into fuel_in_pump(petrol_pump_id, fuel_name)
values (15, 'diesel'),
       (31, '92'),
       (8, '95'),
       (39, '92'),
       (11, '95'),
       (24, 'diesel'),
       (44, '92'),
       (5, 'diesel'),
       (33, '95'),
       (2, '92'),
       (28, '92'),
       (49, 'diesel'),
       (21, '92'),
       (38, '95'),
       (48, 'diesel'),
       (21, 'diesel'),
       (39, '95'),
       (40, '92'),
       (29, 'diesel'),
       (9, '92'),
       (21, '95'),
       (36, '95'),
       (36, 'diesel'),
       (45, '92'),
       (23, '95'),
       (23, '92'),
       (47, 'diesel'),
       (45, '95'),
       (39, 'diesel'),
       (43, 'diesel'),
       (50, 'diesel'),
       (32, 'diesel'),
       (45, 'diesel'),
       (1, 'diesel'),
       (30, 'diesel'),
       (18, '95'),
       (10, '95'),
       (24, '95'),
       (16, 'diesel'),
       (11, '92'),
       (46, '95'),
       (17, '92'),
       (24, '92'),
       (22, '95'),
       (6, '92'),
       (22, '92'),
       (48, '92'),
       (7, '95'),
       (38, 'diesel'),
       (34, '92');

-- fuel presence

insert into fuel_presence(amount, petrol_station_address, fuel_name)
values (176, '441-2655 Velit. St.', '92'),
       (40, '441-2655 Velit. St.', '95'),
       (701, 'Ap #123-6051 Augue Rd.', '95'),
       (289, '783-197 Montes, St.', '95'),
       (530, 'Ap #593-4380 Nunc Avenue', '92'),
       (578, 'Ap #616-6338 Nec Rd.', '92'),
       (886, 'Ap #593-4380 Nunc Avenue', '95'),
       (233, 'Ap #872-9720 Porttitor Road', '92'),
       (155, '441-2655 Velit. St.', 'diesel'),
       (219, 'Ap #728-7257 Tincidunt Avenue', '95'),
       (297, 'P.O. Box 621, 2189 Dictum Street', 'diesel'),
       (535, 'Ap #616-6338 Nec Rd.', '95'),
       (328, '8046 Metus Street', 'diesel'),
       (161, '8046 Metus Street', '95'),
       (943, '8046 Metus Street', '92'),
       (518, 'Ap #123-6051 Augue Rd.', '92'),
       (775, '783-197 Montes, St.', '92'),
       (173, 'Ap #728-7257 Tincidunt Avenue', 'diesel'),
       (629, 'Ap #593-4380 Nunc Avenue', 'diesel'),
       (603, 'Ap #872-9720 Porttitor Road', 'diesel'),
       (787, '783-197 Montes, St.', 'diesel'),
       (523, 'P.O. Box 621, 2189 Dictum Street', '95'),
       (288, 'Ap #123-6051 Augue Rd.', 'diesel'),
       (568, 'P.O. Box 621, 2189 Dictum Street', '92'),
       (673, 'Ap #502-3499 A, St.', 'diesel');