set search_path to project;

-- also we need to find last fuel by name
create index fuel_last_index on fuel(name, valid_to);

-- we often need to find employee by theirs name
create index employees_name_index on employees(name, surname);

-- we often need to know types of fuel in petrol pump
create index petrol_pumps_index on petrol_pumps(petrol_station_address);
create index fuel_in_pump_index on fuel_in_pump(petrol_pump_id);

-- we need to get salary of position by its name
create index positions_name_index on positions(name);