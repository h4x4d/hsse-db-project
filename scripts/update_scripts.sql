set search_path to project;


-- grade up!
update employees
set position_name = case
                      when position_name like '%high level%' then position_name
                      else concat('high level ', position_name)
  end
where name = 'Hilel'
  and surname = 'Haney';

-- close petrol station "Ap #593-4380 Nunc Avenue":
-- we don't want to fire employees, so relocate them to other station

update employees
set petrol_station_address = '783-197 Montes, St.'
where petrol_station_address = 'Ap #593-4380 Nunc Avenue';

-- transfer fuel not to lose it
update fuel_presence as presence
set amount = presence.amount + old.amount
from (select * from fuel_presence where petrol_station_address = 'Ap #593-4380 Nunc Avenue') as old
where presence.petrol_station_address = '8046 Metus Street'
  and presence.fuel_name = old.fuel_name;

-- and then close station
delete
from petrol_stations
where address = 'Ap #593-4380 Nunc Avenue';

-- salaries are rising!
update positions
set salary = case
               when name like '%high level%' then salary + 10000
               else salary + 5000
  end

