create schema if not exists project;

set search_path to project;

create table petrol_stations
(
  address    text primary key,
  open_time  time,
  close_time time,
  has_shop   boolean default false
);

create table positions
(
  name   text primary key,
  salary int not null
);

create table employees
(
  name                   text,
  surname                text,
  position_name          text not null references positions (name) on delete cascade,
  petrol_station_address text not null references petrol_stations (address) on delete cascade on update cascade,
  primary key (name, surname)
);

create table petrol_pumps
(
  id                     serial primary key,
  side                   char(10) not null,
  in_air                 boolean default false,
  petrol_station_address text     not null references petrol_stations (address) on delete cascade on update cascade
);

create table fuel
(
  id         serial primary key,
  name       text,
  fuel_cost  float     not null,
  valid_from timestamp not null,
  valid_to   timestamp not null
);

create table fuel_in_pump
(
  id             serial primary key,
  petrol_pump_id int  not null,
  fuel_name      text not null,
  foreign key (petrol_pump_id) references petrol_pumps (id) on delete cascade
);


create table fuel_presence
(
  id                     serial primary key,
  amount                 float not null,
  petrol_station_address text  not null,
  fuel_name              text  not null
);

alter table fuel_presence
  add constraint fuel_presence_petrol_station_address foreign key
    (petrol_station_address) references petrol_stations (address) on delete cascade on update cascade;