set search_path to project;

-- average salary in petrol station
with salary_employees as (select employees.name, surname, position_name, petrol_station_address, salary
                          from employees
                                 join positions on positions.name = employees.position_name),
     salary_stations as (select petrol_station_address, round(avg(salary) over w) as sum_salary
                         from salary_employees
                         window w as (partition by petrol_station_address)
                         order by petrol_station_address)
select distinct petrol_station_address, sum_salary
from petrol_stations
       left join salary_stations on address = salary_stations.petrol_station_address;
