set search_path to project;

-- summary amount of fuel in petrol stations
with station_amounts as (select petrol_station_address, sum(amount) over w as fuel_presence
                         from fuel_presence
                         window w as (partition by petrol_station_address)
                         order by fuel_presence)
select distinct address, open_time, close_time, has_shop, fuel_presence
from petrol_stations
       left join station_amounts on address = station_amounts.petrol_station_address;
