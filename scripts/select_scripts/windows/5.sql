set search_path to project;

-- left/right pumps on station count
with stations_count as (select distinct side, petrol_station_address, (count(*) over w) as count
                        from petrol_pumps
                        window w as (
                            partition by petrol_station_address, side
                            ))
select petrol_stations.address,
       petrol_stations.open_time,
       petrol_stations.close_time,
       petrol_stations.has_shop,
       left_sided.count  as left_count,
       right_sided.count as right_count
from petrol_stations
       join stations_count left_sided on left_sided.petrol_station_address = address and left_sided.side = 'left'
       join stations_count right_sided on right_sided.petrol_station_address = address and right_sided.side = 'right';