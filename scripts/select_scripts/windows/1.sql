set search_path to project;

-- moving average salary on position
select name, salary, round(avg(salary) over w)
from positions
window w as ( order by name
    rows between 1 preceding and 1 following )
order by name;

