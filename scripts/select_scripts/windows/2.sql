set search_path to project;

-- positions tier list
SELECT dense_rank() OVER w AS dense_rank,
       name,
       salary
FROM positions
WINDOW w AS (
    ORDER BY salary DESC
    )
ORDER BY salary DESC;