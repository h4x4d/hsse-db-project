set search_path to project;

-- select positions with 5 or more employees

select count(*), position_name
from employees
group by position_name
having count(*) >= 5;