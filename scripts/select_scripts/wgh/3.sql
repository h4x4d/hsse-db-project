set search_path to project;

-- select fuel types with average price more than 45 rubles

select name, round(avg(fuel_cost)) as average_cost
from fuel
group by name
having round(avg(fuel_cost)) > 45;