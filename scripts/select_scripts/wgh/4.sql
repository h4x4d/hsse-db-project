set search_path to project;

-- select petrol stations having 1000 or more litres of fuel

select petrol_station_address, sum(amount)
from fuel_presence
group by petrol_station_address
having sum(amount) > 1000;