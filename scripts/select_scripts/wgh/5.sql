set search_path to project;

-- select employees working on petrol stations until 20:00 or longer

select name, surname, position_name, petrol_station_address
from employees
       join petrol_stations on address = petrol_station_address
where close_time >= '20:00';