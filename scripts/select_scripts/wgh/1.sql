set search_path to project;

-- select opening before 8:00 and count that has shops

select count(address), has_shop
from petrol_stations
where open_time < '8:00'
group by has_shop;