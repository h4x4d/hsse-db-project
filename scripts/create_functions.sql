set search_path to project;

create procedure clear_outdated_costs(min_date timestamp)
as
'delete
 from project.fuel
 where valid_to < min_date' language sql;

-- call clear_outdated_costs('2021-01-01');

create procedure grade_up(person_name text, person_surname text)
as
'update project.employees
 set position_name = case
                         when position_name like ''%high level%'' then position_name
                         else concat(''high level '', position_name)
     end
 where name = person_name
   and surname = person_surname;' language sql;

-- call grade_up('Gray', 'Swanson');

create procedure add_fuel(fuel_name text, cost float) as
'update project.fuel
 set valid_to = now()
 where name = fuel_name
   and valid_to > now();
    insert
    into project.fuel(name, fuel_cost, valid_from, valid_to)
    values (fuel_name, cost, now(), ''2999-01-01'');
' language sql;

-- call add_fuel('92', 52.01);


create function check_position_validness() returns trigger as
'begin
if new.salary < 0 then
    raise exception ''salary cannot be less then 0'';
end if;
return new;
end;
' language plpgsql;

create trigger position_update before insert or update on positions
  for each row execute procedure check_position_validness();


-- insert into positions values ('really good volunteer', -1);
-- raises error