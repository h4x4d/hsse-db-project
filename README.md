# HSSE DB PROJECT

## theme: petrol stations

## author: Sidenko Oleg

### Concept model

![concept model](images/concept.jpeg)

### Logical model

![logical model](images/logical.png)

### [Physical model](scripts/create_scripts.sql)
