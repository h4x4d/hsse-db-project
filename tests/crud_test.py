from hamcrest import assert_that

from .models import Position, Employee, PetrolStation, PetrolPump


def test_position(session):
    assert_that(session.query(Position).count() == 0, 'Already have something in the database')

    new_position = Position(name='New position', salary=50000)
    session.add(new_position)
    session.commit()
    assert_that(session.query(Position).count() == 1, 'Not added')

    created_position = session.query(Position).first()
    assert_that(created_position.name == 'New position', 'Name differs')
    assert_that(created_position.salary == 50000, 'Salary differs')

    session.delete(created_position)
    assert_that(session.query(Position).count() == 0, 'Not deleted')


def test_employee(session, position, petrol_station):
    assert_that(session.query(Employee).count() == 0, 'Already have something in the database')

    new_employee = Employee(name='Bob', surname='Smith', position_name=position.name,
                            petrol_station_address=petrol_station.address)
    session.add(new_employee)
    session.commit()
    assert_that(session.query(Employee).count() == 1, 'Not added')

    created_employee = session.query(Employee).first()
    assert_that(created_employee.name == 'Bob', 'Name differs')
    assert_that(created_employee.surname == 'Smith', 'Surname differs')
    assert_that(created_employee.position_name == position.name, 'Position differs')
    assert_that(created_employee.petrol_station_address == petrol_station.address, 'Position differs')

    created_employee.name = 'Jake'
    session.commit()

    same_employee = session.query(Employee).first()
    assert_that(created_employee.name == 'Jake', 'Name not changed')
    assert_that(same_employee.name == 'Jake', 'Name not updated in db')

    session.delete(created_employee)
    assert_that(session.query(Employee).count() == 0, 'Not deleted')


def test_cascading(session):
    p = PetrolStation(address='Test petrol station address',
                      open_time='10:00',
                      close_time='20:00',
                      has_shop=True)
    session.add(p)
    session.commit()
    assert_that(session.query(PetrolStation).count() == 1, 'Petrol station not added')

    session.add(PetrolPump(side='left', petrol_station_address=p.address))
    session.add(PetrolPump(side='right', petrol_station_address=p.address))

    session.commit()
    assert_that(session.query(PetrolPump).count() == 2, 'Pumps not added')

    p.address = 'Test petrol station address moved'
    session.commit()

    assert_that(session.query(PetrolStation).first().address == p.address,
                'Petrol station doesnt updated')
    assert_that(session.query(PetrolPump).first().petrol_station_address == p.address,
                'Petrol pump address doesnt updated')

    session.delete(p)
    session.commit()
    assert_that(session.query(PetrolStation).count() == 0, 'Petrol station not removed')
    assert_that(session.query(PetrolPump).count() == 0, 'Pumps not removed')

