import pytest
from sqlalchemy.orm import Session

from tests.models import Position, PetrolStation, Employee, models, Fuel
from utils.pypsql import sqlalchemy_conn


@pytest.fixture(scope="session")
def engine():
    return sqlalchemy_conn()


@pytest.fixture(scope="session")
def session(engine):
    sess = Session(engine)

    for model in models:
        sess.query(model).delete()

    yield sess

    for model in models:
        sess.query(model).delete()


@pytest.fixture(scope="function")
def position(session):
    p = Position(name='Test position', salary=50000)
    session.add(p)
    session.add(Position(name='high level Test position', salary=100000))
    session.commit()

    yield p

    session.query(Position).delete()


@pytest.fixture(scope="function")
def petrol_station(session):
    p = PetrolStation(address='Test petrol station address',
                      open_time='10:00',
                      close_time='20:00',
                      has_shop=True)
    session.add(p)
    session.commit()

    yield p

    session.delete(p)


@pytest.fixture(scope="function")
def employee(session, position, petrol_station):
    emp = Employee(name='Bob', surname='Smith', position_name=position.name,
                   petrol_station_address=petrol_station.address)
    session.add(emp)
    session.commit()

    yield emp

    session.delete(emp)


@pytest.fixture(scope="function")
def many_fuel(session):
    fuel_data = [Fuel(name=fuel, fuel_cost=50 + i,
                      valid_from=f'202{0 + i}-01-01',
                      valid_to=f'202{1 + i}-01-01') for i in range(5)
                 for fuel in ('92', '95', 'diesel')]
    for i in fuel_data:
        session.add(i)
    session.commit()

    yield fuel_data

    session.query(Fuel).delete()