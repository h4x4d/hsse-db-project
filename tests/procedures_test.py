from datetime import datetime

import pytest
from hamcrest import assert_that, raises, calling
from sqlalchemy.exc import InternalError

from tests.models import Fuel, Position


def test_procedure_outdated(session, many_fuel):
    assert_that(session.query(Fuel).count() == 15, 'not added')
    session.execute("call project.clear_outdated_costs('2023-01-01');")
    session.commit()

    for i in session.query(Fuel).all():
        assert_that(i.valid_to >= datetime(2023, 1, 1), 'not deleted outdated')
        session.delete(i)

    session.commit()


def test_employee_grade_up(session, employee):
    assert_that(employee.position_name == 'Test position', 'Start position differs')
    session.execute('call project.grade_up(:name, :surname)', {"name": employee.name,
                                                               "surname": employee.surname})
    session.commit()
    assert_that(employee.position_name == 'high level Test position', 'Position dont changed')


@pytest.mark.parametrize("fuel_to_change", [("92",), ("95",), ("diesel",)],
                         ids=["92", "95", "diesel"])
@pytest.mark.parametrize("new_cost", [(50.0,), (51.5,), (1000.1,)],
                         ids=["small", "double", "big"])
def test_add_fuel(session, many_fuel, fuel_to_change, new_cost):
    assert_that(session.query(Fuel).count() == 15, 'not added')

    session.execute("call project.add_fuel(:name, :cost)", {"name": fuel_to_change, "cost": new_cost})

    assert_that(session.query(Fuel).count() == 16, 'Not added')
    assert_that(session.query(Fuel).filter(Fuel.name == fuel_to_change).count() == 6, 'Added different')
    assert_that(session.query(Fuel).filter(Fuel.name == fuel_to_change,
                                           Fuel.valid_to > datetime.now()).count() == 1,
                'Affected other data')
    added = session.query(Fuel).filter(Fuel.name == fuel_to_change,
                                       Fuel.valid_to > datetime.now()).first()

    assert_that(added.fuel_cost == new_cost[0], 'Cost differs')


def test_position_validness(session):
    pos = Position(name="test", salary=10000)
    session.add(pos)
    session.commit()
    assert_that(session.query(Position).count() == 1, 'Not added')

    inv_position = Position(name="test invalid", salary=-10000)
    session.add(inv_position)
    assert_that(calling(session.commit), raises(InternalError), 'Has not thrown error on create')
    session.rollback()

    inv_position.salary *= -1
    session.add(inv_position)
    session.commit()
    assert_that(session.query(Position).count() == 2, 'Not added')

    inv_position.salary *= -1
    assert_that(calling(session.commit), raises(InternalError), 'Has not thrown error on change')
    session.rollback()