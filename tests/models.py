from sqlalchemy.ext.automap import automap_base

from utils.pypsql import sqlalchemy_conn

Base = automap_base()


def get_models():
    global Base

    engine = sqlalchemy_conn()
    Base.prepare(engine, schema="project")


get_models()

PetrolStation = Base.classes.petrol_stations
Position = Base.classes.positions
Employee = Base.classes.employees
Fuel = Base.classes.fuel
PetrolPump = Base.classes.petrol_pumps
FuelPresence = Base.classes.fuel_presence
FuelInPump = Base.classes.fuel_in_pump


models = (PetrolStation, Position, Employee, Fuel, PetrolPump, FuelInPump, FuelPresence)